<!DOCTYPE html>
<html>
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>ENVI</title>
      <link href="bootstrap/css/all.css" rel="stylesheet"> <!--load all styles -->
      <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
      <script src="bootstrap/js/bootstrap.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <script src="bootstrap/js/bootstrap.bundle.js"></script>
      <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="estilos.css">

</head>
<body>
	<div class="container-fluid">
              <?php include_once "./header.php"; ?>
              <div class="col col-lg-12 col-md-12 col-sm-12 text-white img_academico">
                  <div class="card-body">
                    <h1 class="card-title">Programas Academicos</h1>
                    <p class="card-text">Explora las acciones que tienes para aprender con ENVI Educación, y como</p> 
                    <p class="card-text">mucho más expandes tus limites.</p> 
                  </div>
              </div>
              <br></br>
              <div class="row">
                  <div class="col col-lg-3 col-md-3 col-sm-3">
                      <nav class="nav flex-column">
                        <a class="nav-link disabled" id="filtrado" href="#" >Filtar por</a>
                        <br>
                        <a class="nav-link disabled tipo_categoria" href="#" >Tipo</a>
                        <a class="nav-link active" href="#">Todos</a>
                        <a class="nav-link disabled" href="#">Cursos</a>
                        <a class="nav-link disabled" href="#">Diplomados</a>
                        <a class="nav-link disabled" href="#">Seminarios</a>
                      </nav>
                      <br>
                      <nav class="nav flex-column">
                        <a class="nav-link disabled tipo_categoria" href="#">Duración</a>
                        <a class="nav-link active" href="#">Todos</a>
                        <a class="nav-link disabled" href="#">6 meses</a>
                        <a class="nav-link disabled" href="#">5 meses</a>
                        <a class="nav-link disabled" href="#">4 meses</a>
                        <a class="nav-link disabled" href="#">3 meses</a>
                        <a class="nav-link disabled" href="#">2 meses</a>
                        <a class="nav-link disabled" href="#">1 meses</a>
                      </nav>
                      <br>
                      <nav class="nav flex-column">
                        <a class="nav-link disabled tipo_categoria" href="#">Categoria</a>
                        <a class="nav-link active" href="#">Todos</a>
                        <a class="nav-link disabled" href="#">Comunicación</a>
                        <a class="nav-link disabled" href="#">Diseño</a>
                        <a class="nav-link disabled" href="#">Admninistración</a>
                        <a class="nav-link disabled" href="#">Marketing</a>
                        <a class="nav-link disabled" href="#">Gastronimia</a>
                        <a class="nav-link disabled" href="#">Contabilidad</a>
                      </nav>
                  </div>
                  <div class="col col-lg-9 col-md-9 col-sm-9">
                     <div class="row">
                        <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado" >
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link" ><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>

                        <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado">
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link"><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>

                        <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado">
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link"><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>

                       <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado">
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link"><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>

                        <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado">
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link"><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>

                        <div class="col col-lg-4 col-md-auto col-sm-auto">
                           <div class="card cartas_filtrado">
                              <img class="card-img-top" src="./img/card6.png" alt="Card image cap">
                              <div class="card-body">
                                 <p class="card-text">PUBLICIDAD APLICADA EN MEDIOS DIGITALES PARA AUMENTAR LAS VENTAS DE TU NEGOCIO</p>
                                 <a href="#" class="card-link"><i class="fas fa-graduation-cap"></i> Curso</a>
                                 <a href="#" class="card-link"><i class="far fa-clock"></i> 6 meses</a>
                                 <br>
                                 <a href="#" class="card-link"><i class="far fa-calendar"></i> Inicio</a>
                                 <a href="#" class="card-link">20/Diciembre/2018</a>
                                 <a href="./detalle_academico" class="btn btn-primary">DETALLE</a>
                              </div>
                          </div>
                        </div>
                  </div>
                    <br>
                    <div class="d-flex justify-content-center">
                      <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            <li class="page-item disabled">
                              <i class="page-link fas fa-caret-left"></i>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                              <span class="page-link">
                                2
                                <span class="sr-only">(current)</span>
                              </span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                              <i class="page-link fas fa-caret-right"></i>
                            </li>
                      </ul>
                    </nav></div>
                </div>
            </div>
              <?php include_once "./footer.php"; ?>
	</div>
</body>
</html>