<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENVI</title>
        <link href="bootstrap/css/all.css" rel="stylesheet"> <!--load all styles -->
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" type="text/css" href="estilos.css">

</head>
<body>
	<div class="container-fluid">
          <?php include_once "./header.php"; ?>
          <div class="col col-lg-12 col-md-12 col-sm-12 text-white img_academico_detalle">
              <div class="card-body">
                  <p class="card-title font-weight-light">Curso Virtual Internacional</p>
                  <h1 class="card-text">Administración de</h1> 
                  <h1 class="card-text">empresas</h1> 
                  <p class="card-text font-weight-normal">Adéntrate en el mundo de la gestión aprende todos los factores</p> 
                  <p class="card-text font-weight-normal" >que intervienen en el éxito de una empresa</p> 
                  <form action="./registrar.php">
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning"><span>Inscribirme</span></button>
                    </div>
                  </form>
              </div>
          </div>
          <br></br>

          <center><div class="separacion_border">
            <h3>Beneficios de estudiar en ENVI</h3>
          </div></center>
          <br></br>
          <div class="col col-lg-5 col-md-3 col-sm-3"></div>
          <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Certificado Internacional</p>
                           <p class="card-text text-center">Al finalizar el curso, diplomado o seminario, acreditamos haberlo concluido satisfactoriamente."</p>
                        </div>
                    </div>
                </div>

                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Disponible las 24h</p>
                           <p class="card-text text-center">Puedes acceder a tus clases las 24h del dia en donde esés.</p>
                        </div>
                    </div>
                </div>

                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Repositorio Educativo</p>
                           <p class="card-text text-center">Puedes acceder a tus clases las 24h del dia en donde estés.</p>
                        </div>
                    </div>
                </div>

                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Profesores del más alto Nivel Académico</p>
                           <p class="card-text text-center">Contamos con profesores del más alto nivel académico, procedentes de diferentes paises con la finalidad de aclarar todas tus dudas.</p>
                        </div>
                    </div>
                </div>

                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Foro Interactivo</p>
                           <p class="card-text text-center">Interactúa en una comunidad de profesionales enfocados en temas de nutrición.</p>
                        </div>
                    </div>
                </div>

                <div class="col col-lg-4 col-md-6 col-sm-12">
                    <div class="card border-0 card_detalles_beneficios">
                        <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                        <br>
                        <div class="card-body">
                           <p class="card-title text-center font-weight-bold">Plataforma Online Didactica e Interactiva</p>
                           <p class="card-text text-center">En ENVI es posible interactuar con tus compañeros y profesores en todo momento.</p>
                           <p class="card-text text-center"><img width="20px" height="20px" src="./img/mexico.jpg" alt="">Ricardo Mejia</p>
                        </div>
                    </div>
                </div>
            </div>
          </div>

          <br></br>
          <center><div class="separacion_border"></div></center>
          <br></br> 

          <div class="row justify-content-start">
            <div class="col col-lg-7 col-md-12 col-sm-12">
              <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                  <a class="nav-link text-success" href="#">DETALLES</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-dark font-weight-bold" href="#">PROGRAMA</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-dark font-weight-bold" href="#">INVERSIÓN</a>
                </li>
              </ul>
            </div>
          </div> 
          <hr class="separacion_border"></hr>

          <br>
          <div class="col col-lg-12 col-md-12 col-sm-12" id="detalles_informacion">
              <nav class="nav flex-column">
                <h3 class="nav-link font-weight-light" href="#">Información General</h3> 
                <div class="row justify-content-end">
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <ul class="list-group">
                              <li class="list-group-item font-weight-bold border-0">Modalidad:</li>
                              <li class="list-group-item font-weight-bold border-0">Inicio:</li>
                              <li class="list-group-item font-weight-bold border-0">Duración:</li>
                              <li class="list-group-item font-weight-bold border-0">Dias:</li>
                              <li class="list-group-item font-weight-bold border-0">Horas de clase:</li>
                            </ul>
                        </div>
                        <div class="col col-lg-4 col-md-3 col-sm-6">
                            <ul class="list-group">
                              <li class="list-group-item border-0">100% En linea</li>
                              <li class="list-group-item border-0">3er Sábado de cada mes</li>
                              <li class="list-group-item border-0">6 meses</li>
                              <li class="list-group-item border-0">Martes y Jueves</li>
                              <li class="list-group-item border-0">A las 16::00 Horas</li>
                            </ul>
                        </div>
                </div>

                <h3 class="nav-link font-weight-light" href="#">Objetivo</h3>
                <div class="row justify-content-end">
                    <div class="col col-lg-7 col-md-7 col-sm-12">
                      <p>Adquirir conocimientos amplios sobre Alimentación y Nutrición</p>
                      <p>enfocada a la hipertrofia muscular, además aprenderás a combinar</p>
                      <p>dosificar y formular dietas que te ayuden a lograr el Desarrollo</p>
                      <p>Muscular.</p>
                    </div>
                </div>

                <h3 class="nav-link font-weight-light" href="#">Aprendizajes Esperados</h3>
                <div class="row justify-content-end">
                    <div class="col col-lg-7 col-md-7 col-sm-12">
                        <ul class="list-group">
                          <li class="list-group-item font-weight-bold border-0"><i class="fas fa-circle"></i> Aprenderá a seleccionar, reconocer y clasificar los alimentos y nutrientes recomendados en el proceso de desarrollo muscular.</li>
                          <li class="list-group-item font-weight-bold border-0"><i class="fas fa-circle"></i> Aprende a reconocer y evaluar tu estado nutricional.</li>
                          <li class="list-group-item font-weight-bold border-0"><i class="fas fa-circle"></i> Aprende a dosificar, combinar y formular dieta anabólica.</li>
                        </ul>
                    </div>
                </div>

                <h3 class="nav-link font-weight-light" href="#">Requisitos</h3>
                <div class="row justify-content-end">
                    <div class="col col-lg-7 col-md-7 col-sm-12">
                        <ul class="list-group">
                          <li class="list-group-item font-weight-bold border-0"><i class="fas fa-circle"></i> Rellenar ficha de inscripción</li>
                          <li class="list-group-item font-weight-bold border-0"><i class="fas fa-circle"></i> Realizar el pago de la primera cuota, más el costo de matrícula.</li>
                        </ul>
                    </div>
                </div>
              </nav>
          </div>

          <div class="col col-lg-12 col-md-12 col-sm-12 contenedor">
           <center>
                <div class="col col-lg-6 col-md-8 col-sm-10 div_text_index">
                    <h1 class="text-white">¡Deseo más información!</h1> 
                    <p class="text-white"> Te haremos llegar mas detalles y recibiras atención personalizada</p>
                </div>
                <div class="col col-lg-8 col-md-8 col-sm-12">
                    <form action="./registrar.php" class="form_index">
                          <input type="email" placeholder="Correo Electrónico" class="form-control" id="exampleInputEmail1">
                          <input type="password" placeholder="Contraseña" class="form-control" id="exampleInputPassword1">
                          <input type="password" placeholder="Repetir Contraseña" class="form-control" id="exampleInputPassword2">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label text-dark" for="customCheck1">He leido y acepto las <u>Politicas y Terminos</u> de Servicio</label>
                        </div>

                        <div class="form-group">
                          <button type="submit" class="btn btn-warning"><span>Registrarme</span></button>
                        </div>
                    </form>
                </div>
            </center>
        </div>
          <div class="col col-lg-12 col-md-12 col-sm-12">
              <br>
              <div class="row">
                <div class="col col-lg-1 col-md-12 col-sm-12"></div>
                <div class="col col-lg-5 col-md-12 col-sm-12">
                    <h2 class="text-left">Opinión de los estudiantes</h2>
                </div>
              </div>
              <br>
              <div class="row justify-content-center">
                  <div class="col col-lg-5 col-md-6 col-sm-12">
                      <div class="card border-0 card_detalles">
                          <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                          <br>
                          <div class="card-body border">
                             <p class="card-text text-center">Cuenta con una excelente plataforma de estudio, te proporciona herramientas y conocimientos de una manera practica y accesible. Además, el plan de estudio es muy completo y facil de llevarlo a cabo, lo recomiendo mucho."</p>
                             <p class="card-text text-center"><img width="20px" height="20px" src="./img/mexico.jpg" alt="">Ricardo Mejia</p>
                          </div>
                      </div>
                  </div>

                  <div class="col col-lg-5 col-md-6 col-sm-12">
                      <div class="card border-0 card_detalles">
                          <center><img src="./img/opinion.png" class="rounded-circle card-img-top" alt="..."></center>
                          <br>
                          <div class="card-body border">
                             <p class="card-text text-center">Cuenta con una excelente plataforma de estudio, te proporciona herramientas y conocimientos de una manera practica y accesible. Además, el plan de estudio es muy completo y facil de llevarlo a cabo, lo recomiendo mucho."</p>
                             <p class="card-text text-center"><img width="20px" height="20px" src="./img/mexico.jpg" alt="">Ricardo Mejia</p>
                          </div>
                      </div>
                  </div>
              </div>
              <br><br>
              <div class="d-flex justify-content-center">
                  <nav aria-label="...">
                      <ul class="pagination pagination-sm">
                          <li class="page-item"><a class="page-link" href="#"></a></li>
                          <li class="page-item active">
                            <span class="page-link">
                              <span class="sr-only">(current)</span>
                            </span>
                          </li>
                          <li class="page-item"><a class="page-link" href="#"></a></li>
                    </ul>
                  </nav>
              </div>
          </div>
          <?php include_once "./footer.php"; ?>
	</div>
</body>
</html>