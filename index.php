<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENVI</title>
        <link href="bootstrap/css/all.css" rel="stylesheet"> <!--load all styles -->
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="js/java.js"></script>
        <link rel="stylesheet" type="text/css" href="estilos.css">

</head>
<body>
	<div class="container-fluid">
        <?php include_once "./header.php"; ?>
        <!--Carousel Wrapper-->
        <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
              <!--Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-2" data-slide-to="1"></li>
                <li data-target="#carousel-example-2" data-slide-to="2"></li>
              </ol>
              <!--/.Indicators-->
              <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <div class="view">
                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg" alt="First slide">
                    <div class="mask rgba-black-light"></div>
                  </div>
                  <div class="carousel-caption carusel_">
                    <div class="col col-lg-12 col-md-12 col-sm-12 text-white">
                      <div class="card-body">
                          <h1 class="card-title h1-responsive">¡Aprender nunca <br> ha sido tan facil</h1>
                          <p class="card-text font-weight-normal">Conoce la gran variedad de programas disponibles para ti</p>
                          <form action="./registrar.php">
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning"><span>Inscribirme</span></button>
                            </div>
                          </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <!--Mask color-->
                  <div class="view">
                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg" alt="Second slide">
                    <div class="mask rgba-black-strong"></div>
                  </div>
                  <div class="carousel-caption">
                    <h3 class="h3-responsive">Strong mask</h3>
                    <p>Secondary text</p>
                  </div>
                </div>
                <div class="carousel-item">
                  <!--Mask color-->
                  <div class="view">
                    <img class="d-block w-8" src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg" alt="Third slide">
                    <div class="mask rgba-black-slight"></div>
                  </div>
                  <div class="carousel-caption">
                    <h3 class="h3-responsive">Slight mask</h3>
                    <p>Third text</p>
                  </div>
                </div>
             </div>
        </div>
        <div class="col col-lg-12 col-md-12 col-sm-12 contenedor">
           <center>
                <div class="col col-lg-6 col-md-8 col-sm-10 div_text_index">
                    <h1 >¡Alcanza tus metas!</h1> 
                    <p >Somos tus aliados en el aprendizaje, supérate a ti mismo</p>
                </div>
                <div class="col col-lg-8 col-md-8 col-sm-12">
                    <form action="./registrar.php" class="form_index">
                          <input type="email" placeholder="Correo Electrónico" class="form-control" id="exampleInputEmail1">
                          <input type="password" placeholder="Contraseña" class="form-control" id="exampleInputPassword1">
                          <input type="password" placeholder="Repetir Contraseña" class="form-control" id="exampleInputPassword2">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label text-dark" for="customCheck1">He leido y acepto las <u>Politicas y Terminos</u> de Servicio</label>
                        </div>

                        <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6Lfuf4kUAAAAAK36_4fQ7RplQ1vyy-3aMr4Vd_8J" style="transform:scale(1.0);transform-origin:0 0;"></div>
                          <button type="submit" class="btn btn-warning"><span>Registrarme</span></button>
                        </div>
                    </form>
                </div>
            </center>
        </div>

        <br><br>
        <center><h1 class="font-weight-light">¡No te pierdas nada de esto!</h1></center>
        <div class="row justify-content-md-center">
              <div class="col col-lg-4 col-md-8 col-sm-12  img_fondo_index">
                    <br><br>
                    <div class="card-body text-left">
                      <h1 class="card-title font-weight-bold">¿ Porque ENVI</h1>
                      <h1 class="card-title font-weight-bold">logra resultados</h1>
                      <h1 class="card-title font-weight-bold">excepcionales?</h1>
                      <p class="card-text">Nuestros programas son diseñados por expertos,</p> 
                      <p class="card-text">apoyados por una institución cuyo único objetivo es</p>
                      <p class="card-text">desarrollar técnicas de enseñanza infalibles, resultado de</p>
                      <p class="card-text">años de estudio y practica</p>

                      <div class="form-group">
                        <button type="submit" class="btn btn-outline-primary my-2 my-sm-0"><span>Quiero saber más</span></button>
                      </div>
                    </div>
              </div>

            <div class="col col-lg-6 col-md-12 col-sm-12">
                 <br><br>
                 <div class="row">
                    <div class="col col-lg-6 col-md-6 col-sm-6">
                       <div class="card border-0 card_index" >
                          <a href="#"><img class="card-img-top"  src="./img/card6.png" alt="Card image cap"></a>
                          <div class="card-body">
                             <p class="card-text font-weight-bold">10 ESTRATEGIAS DE</p>
                             <p class="card-text font-weight-bold">MARKETING INFALIBLES</p>
                          </div>
                      </div>
                    </div>

                    <div class="col col-lg-6 col-md-6 col-sm-6">
                       <div class="card border-0 card_index">
                          <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                          <div class="card-body">
                             <p class="card-text font-weight-bold">HABITOS QUE EVITAN</p>
                             <p class="card-text font-weight-bold">QUE ALCANCES EL EXITO</p>
                             <p class="card-text font-weight-bold">DE TU EMPRESA</p>
                          </div>
                      </div>
                    </div>

                    <div class="col col-lg-6 col-md-6 col-sm-6">
                       <div class="card border-0 card_index">
                          <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                          <div class="card-body">
                             <p class="card-text font-weight-bold">HERRAMIENTAS DE</p>
                             <p class="card-text font-weight-bold">COCINA QUE NO TE</p>
                             <p class="card-text font-weight-bold">PUEDEN FALTAR</p>
                          </div>
                      </div>
                    </div>

                    <div class="col col-lg-6 col-md-6 col-sm-6">
                       <div class="card border-0 card_index">
                          <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                          <div class="card-body">
                             <p class="card-text font-weight-bold">ESTOS ALIMENTOS NO</p>
                             <p class="card-text font-weight-bold">SON TAN SALUDABLES</p>
                             <p class="card-text font-weight-bold">COMO LO PENSABAS</p>
                          </div>
                      </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <br><br>
        <div class="row justify-content-md-center">
            <div class="col col-lg-4 col-md-12 col-sm-12">
              <br><br>
              <center>
                  <h1 class="font-weight-light">Que opinas de los estudiantes?</h1>
                  <div class="card border-0">
                    <img src="./img/opinion.png" style="width: 30%;" class="rounded-circle card-img-top" alt="...">
                    <br>
                    <div class="col col-lg-10 col-md-12 col-sm-12 card-body border">
                       <p class="card-text text-center">Cuenta con una excelente plataforma de estudio, te proporciona herramientas y conocimientos de una manera practica y accesible. Además, el plan de estudio es muy completo y facil de llevarlo a cabo, lo recomiendo mucho."</p>
                       <p class="card-text text-center"><img width="20px" height="20px" src="./img/mexico.jpg" alt="">Ricardo Mejia</p>
                    </div>
                  </div>
                  <br>
              </center>
              <div class="row justify-content-center">
                  <nav aria-label="...">
                    <ul class="pagination pagination-sm">
                      <li class="page-item"><a class="page-link" href="#"></a></li>
                      <li class="page-item active">
                        <span class="page-link">
                          <span class="sr-only">(current)</span>
                        </span>
                      </li>
                      <li class="page-item"><a class="page-link" href="#"></a></li>
                  </ul>
                </nav>
              </div>
          </div>

          <div class="col col-lg-6 col-md-12 col-sm-12">
            <br><br>
              <center>
                <h1 class="font-weight-light">Areas Disponibles</h1>
                   <div class="row">
                      <div class="col col-lg-4 col-md-auto col-sm-auto">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Contabilidad</p>
                            </div>
                        </div>
                      </div>

                      <div class="col col-lg-4 col-md-auto col-sm-auto">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Marketing</p>
                            </div>
                        </div>
                      </div>


                      <div class="col col-lg-4 col-md-auto col-sm-auto">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Diseño</p>
                            </div>
                        </div>
                      </div>

                      <div class="col col-lg-4 col-md-auto col-sm-auto">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Nutrición</p>
                            </div>
                        </div>
                      </div>

                      <div class="col col-lg-4 col-md-auto col-sm-auto">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Tecnología</p>
                            </div>
                        </div>
                      </div>

                      <div class="col col-lg-4 col-md-auto col-sm-12">
                         <div class="card border-0 cartas_index" >
                            <img class="card-img-top" style="width: 50%;" src="./img/card6.png" alt="Card image cap">
                            <div class="card-body">
                               <p class="card-text font-weight-bold">Comunicación</p>
                            </div>
                        </div>
                      </div>
                  </div>
                  <br><br>
                  <h4 class="font-weight-bold">Y mucho mas...</h4>
              </center>
          </div>
        </div>
        <?php include_once "./footer.php"; ?>
        
    </div>
</body>
</html>