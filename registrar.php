<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENVI</title>
        <link href="bootstrap/css/all.css" rel="stylesheet"> <!--load all styles -->
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" type="text/css" href="estilos.css">


</head>
<body>
	<div class="container-fluid">
        <?php include_once "./header.php"; ?>
          <div class="row justify-content-md-center acceso_body">
                <div class="col col-lg-10 col-md-10 col-sm-auto">
                  <br><br>
                  <h1 class="titulo_acceso">¿Ya formas parte de </h1> 
                  <h1 class="titulo_acceso"> ENVI?</h1>
                </div>

                <div class="col col-lg-3 col-md-6 col-sm-12">
                    <nav class="nav">
                        <a class="nav-link text-light link-decoration" href="./login.php">Ingresar</a>
                        <a class="nav-link text-light" href="./registrar.php">Inscribirme</a>
                    </nav>

                    <form class="form_acceso">
                        <div class="form-group">
                          <label class="text-light" for="exampleInputEmail1">Correo Electrónico</label>
                          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>

                        <div class="form-group">
                          <label class="text-light" for="exampleInputPassword1">Contraseña</label>
                          <input type="password" class="form-control" id="exampleInputPassword1">
                        </div>

                        <div class="form-group">
                          <label class="text-light" for="exampleInputPassword2">Repedir Contraseña</label>
                          <input type="password" class="form-control" id="exampleInputPassword2">
                        </div>

                        <div class="form-group">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label text-light" for="customCheck1">He leido y acepto las <u>Politicas y Terminos</u> de Servicio</label>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="g-recaptcha" data-sitekey="6Lfuf4kUAAAAAK36_4fQ7RplQ1vyy-3aMr4Vd_8J" style="transform:scale(1.0);transform-origin:0 0;"></div>
                          <button type="submit" class="btn btn-warning"><span>Ingresar</span></button>
                        </div>
                  </form>
              </div>
          </div>
        <?php include_once "./footer.php"; ?>
        
	</div>
</body>
</html>