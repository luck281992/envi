<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENVI</title>
        <link href="bootstrap/css/all.css" rel="stylesheet"> <!--load all styles -->
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.js"></script>
        <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" type="text/css" href="estilos.css">

</head>
<body>
	<div class="container-fluid">
              <?php include_once "./header.php"; ?>
                <br><br>
                 <div class="row justify-content-md-center">
                    <div class="col col-lg-2 col-md-auto col-sm-auto">
                       <h4><i class="fas fa-th"></i> Categorias</h4>
                    </div>
                 </div>
                 <br><br>
              
                 <div class="row justify-content-md-center">
                      <div class="col col-lg-6 col-md-12 col-sm-12 text-white">
                          <img src='./img/admin.jpeg' class="card-img blog-img1" alt="...">
                          <div class="card-img-overlay">
                              <p class="card-title">Administración de negocios</p>
                              <h3 class="card-text">¿Por que es importante </h3>
                              <h3 class="card-text">llevar un control</h3>
                              <h3 class="card-text">de ventas?</h3>
                          </div>
                      </div>

                      <div class="col col-lg-6 col-md-12 col-sm-12 text-white">
                          <div  class="col col-lg-12 col-md-12 col-sm-12 text-white">
                              <img class="card-img blog-img2" src='./img/marketing.jpg' alt="...">
                              <div class="card-img-overlay">
                                  <p class="card-title">Marketing</p>
                                  <h3 class="card-text">Descubre como atrae</h3> 
                                  <h3 class="card-text">mas clientes</h3>
                              </div>
                          </div>
                          <br>
                          <div class="col col-lg-12 col-md-12 col-sm-12 text-white">
                              <img src='./img/tecnologia.jpg' class="card-img blog-img2" alt="...">
                              <div class="card-img-overlay">
                                  <p class="card-title">Tecnologia</p>
                                  <h3 class="card-text">Métodos infalibres para ser</h3>
                                  <h3 class="card-text"> productivo en tu trabajo</h3>
                              </div>
                          </div>
                      </div>
                </div>
                <br><br>                
                <center><h3>¿Que estas buscando?</h3>
                <br><br>
                <div class="col col-lg-10 col-md-12 col-sm-12"></div>
                <div class="col col-lg-6 col-md-7 col-sm-8">
                <!-- Search form -->
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text bg-white border-top-0 border-left-0"><i class="fa fa-search"></i></span>
                      </div>
                      <input type="search" placeholder="Search" class="form-control border-left-0 border-top-0 border-right-0">
                    </div>
                </div></center>
                <br><br>
                <div class="row justify-content-md-center">
                    <div class="col col-lg-8 col-m-8 col-sm-12">
                         <div class="row">
                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>

                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>

                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>

                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>

                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>

                            <div class="col col-lg-5 col-md-4 col-sm-6">
                               <div class="card cartas_blog">
                                  <a href="#"><img class="card-img-top" src="./img/card6.png" alt="Card image cap"></a>
                                  <div class="card-body">
                                     <p class="card-text text-blog">Titulo para una entrada en especifico del blog</p>
                                  </div>
                              </div>
                            </div>
                        </div>

                        <br>
                        <div class="d-flex justify-content-center">
                            <nav aria-label="...">
                              <ul class="pagination pagination-sm">
                                  <li class="page-item disabled">
                                    <i class="page-link fas fa-caret-left"></i>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                                  <li class="page-item active">
                                    <span class="page-link">
                                      2
                                      <span class="sr-only">(current)</span>
                                    </span>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                                  <li class="page-item">
                                    <i class="page-link fas fa-caret-right"></i>
                                  </li>
                            </ul>
                          </nav>
                        </div>

                    </div>

                    <div class="col col-lg-3 col-md-12 col-sm-3">
                        <nav class="nav flex-column">
                            <a href="#"><img class="card-img-top publicidad" src="./img/card6.png" alt="Card image cap"></a>
                            <a href="#"><img class="card-img-top publicidad" src="./img/card6.png" alt="Card image cap"></a>
                        </nav>
                    </div>

              </div>

              <?php include_once "./footer.php"; ?>
	</div>
</body>
</html>