<!DOCTYPE html>
<html>
<head>
	<title>	
	</title>
</head>
<body>
	<div class="container-fluid">
		<hr></hr>
		<div class="row">
			<div class="col col-lg-4 col-md-4 col-sm-4">
				<nav class="nav flex-column">
				  <a class="nav-link active" href="#">ENVI Educación</a>
				  <a class="nav-link disabled" href="#">Cursos</a>
				  <a class="nav-link disabled" href="#">Diplomados</a>
				  <a class="nav-link disabled" href="#">Seminarios</a>
				  <a class="nav-link disabled" href="#">Quienes somos</a>
				  <a class="nav-link disabled" href="#">Blog</a>
				</nav>
			</div>

		    <div class="col col-lg-4 col-md-3 col-sm-4">
				<nav class="nav flex-column">
				  <a class="nav-link active" href="#">Ayuda</a>
				  <a class="nav-link disabled" href="#">Terminos y condiciones</a>
				  <a class="nav-link disabled" href="#">Preguntas frecuentes</a>
				  <a class="nav-link disabled" href="#">Uso y manejo de datos privados</a>
				  <a class="nav-link disabled" href="#">Mapa del sitio</a>
				</nav>
			</div>

			<div class="col col-lg-4 col-md-5 col-sm-4">
				<nav class="nav flex-column">
				    <a class="nav-link active" href="#">Redes Sociales</a>
				    <nav class="nav">
						<a class="nav-link disabled redes_sociales" href="#"><i class="fab fa-facebook-square"></i></a>
						<a class="nav-link disabled redes_sociales" href="#"><i class="fab fa-youtube"></i></a>
						<a class="nav-link disabled redes_sociales" href="#"><i class="fab fa-instagram"></i></a>
						<a class="nav-link disabled redes_sociales" href="#"><i class="fab fa-twitter"></i></a>
				    </nav>
				</nav>
			</div>
		</div>
		<hr></hr>        
		<div class="row"> 
			<div class="col col-lg-4 col-md-auto col-sm-auto">
			<p class="text-muted small">@ Todos los derechos reservados</p>
			<p class="text-muted small" id="text-copyright">ENVI Educación</p>
		</div>

	</div>
</body>
</html>