<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav md-auto">
                        <li class="nav-item"><a class="navbar-brand" href="./"><img height="40px" class="d-block w-90" src="./img/envi.png" alt="First slide"></a></li>
                        <li class="nav-item"><a class="nav-link" href="./academico.php">Programas Academicos</a></li>
                        <li class="nav-item"><a class="nav-link" href="./blog">Blog</a></li>
                        <li class="nav-item"><a class="nav-link" href="./contacto">Quienes Somos</a></li>
                        <li class="nav-item">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img width="20px" height="20px" src="./img/mexico.jpg" alt="">+52 123 456 7890</a>
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <a class="dropdown-item" href="#">Something else here</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Separated link</a>
                            </div>
                      </li>
                      <li id="acceso">
                        <form class="form-inline" action="./login.php">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"><i class="fas fa-lock"></i>Acceso de Estudiantes</button>
                    </form></li>
                    </ul>
                </div>
            </nav>
        </div>
   </body>
</html>
